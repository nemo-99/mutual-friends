# Mutual Friends Algorithm in pySpark

## The Problem

- The file soc-LiveJournal1Adj.txt contains Users and their corresponding Friends in the following format

``` User [Tab] Friend1, Friend2 , Friend3 ... ```

- The task challenge is to generate top 10 friends recommendations for each user based on the highest mutual friend count between two users
- However, the recommendations must also not be direct friends

## The Solution

A brief overview of my approach is as follows -

- Loaded the data into a pandas dataframe to make use of its rich functionalities.
- Used the pandas explode function to convert the data into the following format
   ```
    User1 Friend1
    User1 Friend2
    ...
    User2 Friend1
  ```
- In the next step, I initialised a spark session, and loaded the above data into a spark dataframe
- I then applied a self-join on the above dataframe, creating two aliases ```df1``` and ```df2```, and joining on ```df1.Friends == df2.Users```
- This gave me the first degree connections of each user. However, this also contains direct friends, and user - user entries which have to be filtered out
- I then applied a groupBy operation on the above dataframe, aggregating on each ```User,Friend``` entry
- As this point, I apply a left-anti join with the original dataframe, eliminating all direct friends from the dataframe, leaving only true friend recommendations. I also filter out the erroneous user-user records.
- I now partition the data-frame based on ```User``` record, ordering on the count.
- I finally assign a row_number to each record based on the count, which allows me to filter and get the top 10 friend recommendations.
- Now I can optionally additionally filter through the records, querying for a specific user
- Finally, I transfer the data to a pandas dataframe so I can store it to a file in a specific format
